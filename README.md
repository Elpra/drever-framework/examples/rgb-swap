# RGB Swap

This is a simple Drever example for a RGB color channel swap video algorithm. It reorders the single color channels as

| Input | Output |
|-------|--------|
| R     | G      |
| G     | B      |
| B     | R      |
