'''
Created on May 24, 2019

@author: baumannt
'''
import unittest
import imageio
import numpy as np
from drever.data_handlers.image import ImageData
from rgb_swap.rgb_swap import RGBSwap


class Test(unittest.TestCase):


    def test_swap_runner(self):

        test_card_file = "test_card.png"
        test_card_data = imageio.imread(test_card_file)[::,::,:3]
        input_data = ImageData()
        input_data.init_with_data(test_card_data.astype(np.uint16))

        uut_params = {"SWAP_R": 1, "SWAP_G": 2, "SWAP_B": 0}

        uut = RGBSwap(uut_params, input_data, None)
        uut.run()

        output_data = uut.get_output_data()

        imageio.imwrite("test_card_processed.png", output_data.image_data.astype(np.uint8))

if __name__ == "__main__":  # pragma: no cover
    unittest.main()
