'''
Created on May 24, 2019

@author: baumannt
'''
import numpy as np
from drever import video_filter
from drever.data_handlers.image import ImageData


class RGBSwap(video_filter.VideoFilter):
    '''
    classdocs
    '''

    def run(self):

        swapped_data = ImageData()
        swapped_data.init_with_data(
            np.zeros(self.get_input_data().get_np_shape(), dtype=np.uint16),
            True,
            8
        )

        swap_order = [
            self.get_params()["SWAP_R"],
            self.get_params()["SWAP_G"],
            self.get_params()["SWAP_B"],
        ]

        for i in range(0, 3):
            swapped_data[::, ::, i] = self.get_input_data()[::, ::, swap_order[i]]

        self._set_output_data(swapped_data)
