library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

library vunit_lib;
context vunit_lib.vunit_context;

library osvvm;
use osvvm.CoveragePkg.all;

library drever;
use drever.image_data_pkg.all;

library rgb_swap_lib;

entity tb_rgb_swap is
	generic (
		runner_cfg   : string := runner_cfg_default;
		output_path  : string;
		IN_FILENAME  : string;
		OUT_FILENAME : string;
		UUT_PARAMS   : string;
		USE_OSVVM    : boolean
	);
end entity tb_rgb_swap;

architecture RTL of tb_rgb_swap is

	constant BITDEPTH : natural := 8;

	signal clk          : std_logic;
	signal source_valid : std_logic                               := '0';
	signal source_video : std_logic_vector(3*BITDEPTH-1 downto 0) := (others => '0');
	signal uut_valid    : std_logic                               := '0';
	signal uut_video    : std_logic_vector(3*BITDEPTH-1 downto 0) := (others => '0');

	shared variable source_image : t_image;
	shared variable sink_image   : t_image;
	shared variable cov_source   : covPType;

	signal done : boolean := false;

	function conv_ch_index(i : natural) return natural is
	begin
		assert i >= 0 and i < 3
			report "Index out of range!"
			severity failure;
		return 2-i;
	end function;

begin

	gen_clk_proc : process
	begin
		clk <= '0';
		wait for 5 ns;
		clk <= '1';
		wait for 5 ns;
	end process;

	test_runner : process
		variable params : t_params;
	begin

		test_runner_setup(runner, runner_cfg);
		sink_image.load(output_path & "/" & OUT_FILENAME, 0);
		wait until done;
		test_runner_cleanup(runner);

	end process;

	video_source_proc : process
		variable params : t_params;
		variable ch     : natural;
	begin

		source_image.load(output_path & "/" & IN_FILENAME, 0);
		params := source_image.get_params;

		cov_source.AddCross(
			params.channels,
			GenBin(0, params.height-1), GenBin(0, params.width-1)
		);

		source_valid <= '0';
		wait for 100 ns;

		for y in 0 to params.height-1 loop

			for x in 0 to params.width-1 loop

				wait until rising_edge(clk);
				source_valid <= '1';

				for i in 0 to params.channels-1 loop

					if (USE_OSVVM) then
						cov_source.ICover((y, x));
					end if;

					ch := conv_ch_index(i);
					source_video((i+1)*BITDEPTH-1 downto i*BITDEPTH) <=
						std_logic_vector(to_unsigned(source_image.get_entry(x, y, ch), BITDEPTH));

				end loop;

			end loop;

			wait until rising_edge(clk);
			source_valid <= '0';
			wait for 100 ns;

		end loop;

		if (USE_OSVVM) then
			cov_source.WriteCovHoles;
			info("Total Coverage " & integer'image(integer(floor(cov_source.GetCov))) & "%");
		end if;

		done <= true;

		wait;

	end process;

	uut : entity rgb_swap_lib.rgb_swap
		generic map (
			BITDEPTH => BITDEPTH,
			SWAP_R   => conv_ch_index(natural'value(get(UUT_PARAMS, "SWAP_R"))),
			SWAP_G   => conv_ch_index(natural'value(get(UUT_PARAMS, "SWAP_G"))),
			SWAP_B   => conv_ch_index(natural'value(get(UUT_PARAMS, "SWAP_B")))
		)
		port map (
			clk_in   => clk,
			valid_in => source_valid,
			video_in => source_video,
			valid_q  => uut_valid,
			video_q  => uut_video
		);

	video_sink_proc : process (clk)
		variable params    : t_params;
		variable x, y      : integer := 0;
		variable was_valid : boolean := false;
		variable ch        : natural;
	begin

		if rising_edge(clk) then

			if uut_valid = '1' then

				was_valid := true;

				for i in 0 to 2 loop
					ch := conv_ch_index(i);
					assert unsigned(uut_video((i+1)*BITDEPTH-1 downto i*BITDEPTH)) = sink_image.get_entry(x, y, ch)
						report "Video Data Missmatch" & LF &
							"Entry X/Y/Ch: " & integer'image(x) & "/" & integer'image(y) & "/" & integer'image(ch) & LF &
							"Is value:     " & integer'image(to_integer(unsigned(uut_video((i+1)*BITDEPTH-1 downto i*BITDEPTH)))) & LF &
							"Exp. value:   " & integer'image(sink_image.get_entry(x, y, ch))
						severity error;
				end loop;

				x := x + 1;

			else

				if was_valid then
					y := y + 1;
				end if;

				was_valid := false;
				x         := 0;

				assert unsigned(uut_video) = 0
					report "Video is not Null"
					severity error;

			end if;

		end if;

	end process;

end architecture RTL;
