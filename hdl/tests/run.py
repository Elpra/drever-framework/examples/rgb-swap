from vunit import VUnit
import os
import imageio
import numpy as np

import drever
from drever.data_handlers.image import ImageData
import rgb_swap
from rgb_swap.rgb_swap import RGBSwap


# Create VUnit instance by parsing command line arguments
vu = VUnit.from_argv()

# Create libraries
vu.add_osvvm()
drever.add_drever_hdl_sources(vu)
vhdl_rtl_dir = os.path.join(rgb_swap.__path__[0], "../../hdl/rtl/*.vhd")
vhdl_tests_dir = os.path.join(rgb_swap.__path__[0], "../../hdl/tests/*.vhd")

rgb_swap_lib = vu.add_library("rgb_swap_lib")
rgb_swap_lib.add_source_files(vhdl_rtl_dir)

# Testbenches
test_lib = vu.add_library("test_lib")
test_lib.add_source_files(vhdl_tests_dir)
tb_rgb_swap = test_lib.entity("tb_rgb_swap")

random_data = ImageData({
    "width":    200,
    "height":   100,
    "bitdepth":  8,
    "channels":  3
})
random_data.init_with_random(0)

test_card_file = "test_card.png"
test_card_pixels = imageio.imread(test_card_file)[::,::,:3]
test_card_data = ImageData()
test_card_data.init_with_data(test_card_pixels.astype(np.uint16), True, 8)

uuts = [
    RGBSwap(
        params={"SWAP_R": 1, "SWAP_G": 2, "SWAP_B": 0},
        input_data=random_data,
        dump_filename="random120.ppm"
    ),
    RGBSwap(
        params={"SWAP_R": 0, "SWAP_G": 0, "SWAP_B": 0},
        input_data=random_data,
        dump_filename="random000.ppm"
    ),
    RGBSwap(
        params={"SWAP_R": 0, "SWAP_G": 1, "SWAP_B": 2},
        input_data=random_data,
        dump_filename="random012.ppm"
    ),
    RGBSwap(
        params={"SWAP_R": 1, "SWAP_G": 2, "SWAP_B": 0},
        input_data=test_card_data,
        dump_filename="test_card.ppm"
    )
]

for uut in uuts:

    config_name = "dumpfile=%s" % (uut.dump_filename)

    params = uut.params

    generics = uut.vunit_generate_generics()
    generics["UUT_PARAMS"] = \
        ", ".join(["%s:%s" % (key, str(params[key])) for key in params])
    generics["USE_OSVVM"] = True if uut.dump_filename == "random120.ppm" else False

    tb_rgb_swap.add_config(
        name="rgb_swap:" + config_name,
        pre_config=uut.vunit_pre_config,
        generics=generics
    )


vu.main()
