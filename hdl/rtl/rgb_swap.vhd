library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity rgb_swap is
	generic(
		BITDEPTH : natural range 0 to 16 := 8;
		SWAP_R   : natural range 0 to  2 := 0;
		SWAP_G   : natural range 0 to  2 := 0;
		SWAP_B   : natural range 0 to  2 := 0
	);
	port(
		clk_in   : in  std_logic;
		valid_in : in  std_logic;
		video_in : in  std_logic_vector(3*BITDEPTH-1 downto 0);
		valid_q  : out std_logic;
		video_q  : out std_logic_vector(3*BITDEPTH-1 downto 0)
	);
end entity rgb_swap;

architecture RTL of rgb_swap is

	signal valid : std_logic                       := '0';
	signal video : std_logic_vector(video_q'range) := (others => '0');

	type t_int_list is array (0 to 2) of natural;
	constant SWAP_LIST : t_int_list := (
		0 => SWAP_B,
		1 => SWAP_G,
		2 => SWAP_R
	);

begin

	valid_q <= valid;
	video_q <= video;

	swap_proc : process (clk_in)
	begin

		if (rising_edge(clk_in)) then

			if valid_in = '1' then

				valid <= '1';

				for i in 0 to 2 loop
					video(BITDEPTH*(i+1)-1 downto BITDEPTH*i) <=
						video_in(BITDEPTH*(SWAP_LIST(i)+1)-1 downto BITDEPTH*SWAP_LIST(i));
				end loop;

			else
				valid <= '0';
				video <= (others => '0');
			end if;

		end if;

	end process;

end architecture RTL;
